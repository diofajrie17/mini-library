package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping
    public List<Book> getAllBooks() {
        return bookService.getAllBooks();
    }

    @PostMapping("/saveBooks")
    public ResponseEntity<String> saveBooks(@RequestBody List<Book> books) {
        bookService.saveBooks(books);
        return ResponseEntity.ok("Books saved successfully");
    }

    @GetMapping("/byAuthor/{author}")
    public List<Book> getBooksByAuthor(@PathVariable String author) {
        return bookService.getBooksByAuthor(author);
    }

    @PostMapping("/addSampleData")
    public void addSampleData(){
        bookService.addSampleData();
    }

    @GetMapping("/byTitlePrefix/{prefix}")
    public List<Book> getByTitlePrefix(@PathVariable String prefix) {
        return bookService.getByTitlePrefix(prefix);
    }
}
