package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public void saveBooks(List<Book> books) {
        books.stream().forEach(bookRepository::save);
    }

    public List<Book> getBooksByAuthor(String author) {
        return bookRepository.findByAuthor(author);
    }

    public void addSampleData() {
        Book book3 = new Book("hari poah", "andrea");
        Book book2 = new Book("mad man", "bobi");

        bookRepository.saveAll(List.of(book3, book2));
    }

    public List<Book> getByTitlePrefix(String prefix) {
        List<Book> allBooks = bookRepository.findAll();

        return allBooks.stream()
                .filter(book -> book.getTitle().startsWith(prefix))
                .collect(Collectors.toList());
    }
}
